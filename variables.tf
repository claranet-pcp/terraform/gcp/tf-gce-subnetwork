variable "envname"            {}
variable "envtype"            {}
variable "network_name"       {}
variable "public_subnet_cidr" {}
variable "public_subnet_name" {}
variable "project"            {}

variable "public_fw_cidrs" {
  type = "list"
}

variable "gce_region" {
  default = "europe-west1"
}

/* Value of the europe-west1 key have to be string otherwise lookup() is not
 * going to work
 */
variable "gce_zones" {
  default = {
    europe-west1 = "europe-west1-b,europe-west1-c,europe-west1-d"
  }
}

variable "nat_gw_image" {
  default = "centos-7"
}

variable "nat_boxes_count" {
  default = 1
}
