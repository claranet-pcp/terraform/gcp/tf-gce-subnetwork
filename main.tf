#
# Public Subnets
#
resource "google_compute_subnetwork" "public" {
  name          = "${var.public_subnet_name}"
  ip_cidr_range = "${var.public_subnet_cidr}"
  network       = "${var.network_name}"
  region        = "${var.gce_region}"
}

resource "google_compute_route" "public_default" {
  name                   = "${var.public_subnet_name}-default-route"
  dest_range             = "0.0.0.0/0"
  network                = "${var.network_name}"
  next_hop_instance_zone = "${element(split(",", lookup(var.gce_zones, var.gce_region)), count.index)}"
  next_hop_gateway       = "projects/${var.project}/global/gateways/default-internet-gateway"
  priority               = 100
}

#
# Bastion for accessing environment
#
resource "google_compute_address" "nataddress" {
  count = "${var.nat_boxes_count}"
  name  = "nat${count.index}address"
}

resource "google_compute_instance" "bastion" {
  count          = "${var.nat_boxes_count}"
  name           = "bastion-${count.index}"
  machine_type   = "g1-small"
  zone           = "${element(split(",", lookup(var.gce_zones, var.gce_region)), count.index)}"
  can_ip_forward = "true"

  tags = [
    "${var.envname}",
    "${var.envtype}",
    "global",
  ]

  disk {
    image = "${var.nat_gw_image}"
    type  = "pd-ssd"
  }

  network_interface {
    subnetwork = "${google_compute_subnetwork.public.name}"

    access_config {
      nat_ip = "${element(google_compute_address.nataddress.*.address, count.index)}"
    }
  }
}
