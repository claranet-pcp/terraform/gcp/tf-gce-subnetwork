output "public_subnet_name" {
  value = "${google_compute_subnetwork.public.name}"
}

output "public_subnet_link" {
  value = "${google_compute_subnetwork.public.self_link}"
}
